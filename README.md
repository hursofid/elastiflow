Elasticsearch + Logstash + Kibana + Elastiflow on AWS with Terraform and Ansible
================================================================================

```
+--------------------------------------------------------------------------------------+
|                                                                                      |
|   THIS IS OLD DOCUMENT VERSION DEVELOPED SOME WHILE AGO, I'M GONNA UPDATE IT SOON    |
|                                                                                      |
+--------------------------------------------------------------------------------------+
```

ElastiFlow™ provides network flow data collection and visualization using the Elastic Stack (Elasticsearch, Logstash and Kibana). It supports Netflow v5/v9, sFlow and IPFIX flow types (1.x versions support only Netflow v5/v9).

• Spin out AWS EC2 instance in dedicated VPC in /28 network in eu-north-1 region

• Basic system configuration
Installs NTP, net-tools, htop, nano and curl

• Installs ELK Stack with Elastiflow
Elasticsearch runs on localhost on 9200/tcp
Kibana runs on 5602/tcp
Accepting NetFlow input to 2055/udp

• Optionally configures firewall with INPUT DROP policy - adjust according to your needs


Pre-requisites:
-------------

• This playbook tested with Debian 10

• 5 GB RAM - adjust according to your needs

• Add your IP address to files/hosts.allow so you can access target host after provision

Instructions:
-------------

• Add an entry with target host to your Ansible inventory file /etc/ansible/hosts or /usr/local/ansible/hosts depending on your Ansible configuration

• Create and run playbook with following content:

```
- hosts: elastiflow-host
  roles:
  - { role: elastiflow }
```

• Run with `ansible-playbook playbook-name.yml`

To-do:
-------------

• Add support for other distros than Debian

• Use jinja2 templates instead of lineinfile module with_items

• Register facts if already installed to avoid running 'shell' module tasks again if not really necessarily

• Handlers

Known issues:
-------------

Fetching private key generated on target host with too open permissions - there is no way to set permissions using fetch module as for now (Ansible 2.7)
