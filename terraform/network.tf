resource "aws_vpc" "elastiflow" {
  cidr_block = var.vpc_main_cidr_block
  tags = {
    Name = join("-", [var.project_name, var.environment])
  }
}

resource "aws_eip" "ec2" {
  instance = aws_instance.ec2.id
  vpc = true
  tags = {
    Name = join("-", [var.project_name, var.environment])
    Environment = var.environment
  }
}

resource "aws_subnet" "main" {
  vpc_id = aws_vpc.elastiflow.id
  cidr_block = var.vpc_main_cidr_block
  tags = {
    Name = "main"
    Environment = var.environment
  }
}

resource "aws_internet_gateway" "elastiflow" {
  vpc_id = aws_vpc.elastiflow.id
}

resource "aws_route_table" "main" {
  vpc_id = aws_vpc.elastiflow.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.elastiflow.id
  }
  tags = {
    Name = "main"
    Environment = var.environment
  }
}

resource "aws_route_table_association" "main" {
  subnet_id = aws_subnet.main.id
  route_table_id = aws_route_table.main.id
}
