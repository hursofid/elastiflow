data "aws_ami" "ec2" {
  owners = ["aws-marketplace"]
  filter {
    name = "image-id"
    values = [var.ami_id]
  }
}

resource "aws_instance" "ec2" {
  ami = data.aws_ami.ec2.id
  instance_type = "t3.large"
  vpc_security_group_ids = ["${aws_security_group.all_rules.id}"]
  subnet_id = aws_subnet.main.id
  key_name = var.key_name
  tags = {
    Name = "ec2"
    Project = var.project_name
    Environment = var.environment
  }
}
