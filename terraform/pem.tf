resource "tls_private_key" "elastiflow" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "elastiflow" {
  key_name   = var.key_name
  public_key = tls_private_key.elastiflow.public_key_openssh
}
