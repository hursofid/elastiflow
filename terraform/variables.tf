variable "key_name" {
  description = "OpenSSH private key name"
  default = "elastiflow"
}

variable "aws_region" {
  default = "eu-north-1"
}

variable "project_name" {
  default = "elastiflow"
}

variable "aws_profile" {
  default = "personal"
}

variable "vpc_main_cidr_block" {
  default = "10.0.30.0/28"
}

variable "environment" {
  default = "dev"
}

variable "ami_id" {
  default = "ami-037672bdf0aed843a"
}
