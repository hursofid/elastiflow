provider "aws" {
  region = var.aws_region
  shared_credentials_file = "~/.aws/credentials"
  profile = "personal"
}

terraform {
  required_version = ">=0.12.28"
}

locals {
  project_name = var.project_name
  environment = var.environment
}
